<?php
namespace Arm\Core\Tests\Unit\Infrastructure;

use Arm\Core\Application\Query\ItemQuery;
use Arm\Core\Infrastructure\Doctrine\DBAL\Query\DbalItemView;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;

class DbalItemViewTest extends AItemViewTestCase
{
    /**
     * @return ItemQuery
     */
    protected function buildTestSet1Query(): ItemQuery
    {
        $connection = $this->_buildConnection();
        $this->_initDb($connection);
        $connection->query('INSERT INTO arm_item_type VALUES("type1", "typecode1");');
        $connection->query('INSERT INTO arm_item_type VALUES("type2", "typecode2");');
        $connection->query('INSERT INTO arm_item VALUES("id1", "code11", "type1");');
        $connection->query('INSERT INTO arm_item VALUES("id2", "code12", "type1");');
        $connection->query('INSERT INTO arm_item VALUES("id3", "code21", "type2");');
        $query = new DbalItemView($connection);
        return $query;
    }


    protected function _initDb(Connection $connection)
    {
        $connection->query('
CREATE TABLE arm_item
(
    id VARCHAR(50) PRIMARY KEY NOT NULL,
    code VARCHAR(50) NOT NULL,
    type_id VARCHAR(50) NOT NULL
);');
        $connection->query('
CREATE TABLE arm_item_type
(
    id STRING PRIMARY KEY NOT NULL,
    code STRING NOT NULL
);
');
    }

    /**
     * @return Connection
     */
    protected function _buildConnection()
    {
        $config = new Configuration();
        $connectionParams = array(
            'url' => 'sqlite:///:memory:'
        );
        $conn = DriverManager::getConnection($connectionParams, $config);
        return $conn;
    }
}
