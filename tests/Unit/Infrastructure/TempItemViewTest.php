<?php
namespace Arm\Core\Tests\Unit\Infrastructure;

use Arm\Core\Application\Query\Item;
use Arm\Core\Application\Query\ItemQuery;
use Arm\Core\Infrastructure\Memory\Items;
use Arm\Core\Infrastructure\Memory\Query\TempItemView;

class TempItemViewTest extends AItemViewTestCase
{
    /**
     * @return ItemQuery
     */
    protected function buildTestSet1Query(): ItemQuery
    {
        $items = new Items();
        $items->push(new Item('id1', 'code11', 'typecode1', []));
        $items->push(new Item('id2', 'code12', 'typecode1', []));
        $items->push(new Item('id3', 'code21', 'typecode2', []));
        $query = new TempItemView($items);
        return $query;
    }

}
