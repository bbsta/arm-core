<?php
namespace Arm\Core\Tests\Unit\Infrastructure;

use Arm\Core\Application\Query\Item;
use Arm\Core\Application\Query\ItemFilter;
use Arm\Core\Application\Query\ItemQuery;
use PHPUnit\Framework\TestCase;

abstract class AItemViewTestCase extends TestCase
{
    public function testBase()
    {
        $query = $this->buildTestSet1Query();
        $this->assertEquals(3, $query->itemCount());
        $item = $query->itemByCode('code12');
        $this->assertInstanceOf(Item::class, $item);
        $this->assertEquals('code12', $item->code());
        $this->assertEquals('typecode1', $item->type());
    }

    public function testFilters()
    {
        $query = $this->buildTestSet1Query();
        $this->assertEquals(3, $query->itemCount());
        $filter = new ItemFilter();
        $filter = $filter->withLimit(2);
        $items = $query->find($filter);
        $this->assertContainsOnly(Item::class, $items);
        $this->assertCount(2, $items);
    }

    /**
     * @return ItemQuery
     */
    abstract protected function buildTestSet1Query(): ItemQuery;

}