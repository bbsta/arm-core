<?php
namespace Arm\Core\Infrastructure\Memory;

use Arm\Core\Application\Query\Item;

class Items
{
    /**
     * @var iterable|Item[]
     */
    private $items = [];

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->items);
    }

    /**
     * @return Item[]|iterable
     */
    public function all()
    {
        return $this->items;
    }

    /**
     * @param Item $item
     */
    public function push(Item $item)
    {
        $this->items[] = $item;
    }
}
