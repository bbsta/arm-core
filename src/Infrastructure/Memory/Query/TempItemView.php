<?php
namespace Arm\Core\Infrastructure\Memory\Query;

use Arm\Core\Application\Query\Item;
use Arm\Core\Application\Query\ItemFilter;
use Arm\Core\Application\Query\ItemQuery;
use Arm\Core\Infrastructure\Memory\Items;

class TempItemView implements ItemQuery
{
    /**
     * @var Items
     */
    private $items;

    /**
     * TempItemView constructor.
     * @param Items $items
     */
    public function __construct(Items $items)
    {
        $this->items = $items;
    }

    /**
     * @return int
     */
    public function itemCount(): int
    {
        return $this->items->count();
    }

    /**
     * @param string $code
     * @return Item
     * @throws \Exception
     */
    public function itemByCode(string $code): Item
    {
        foreach ($this->items->all() as $item) {
            if ($item->code() == $code) {
                return $item;
            }
        }
        throw new \Exception();
    }

    /**
     * @param ItemFilter $filter
     * @return iterable
     */
    public function find(ItemFilter $filter): iterable
    {
        $offset = $filter->offset();
        $limit = $filter->limit();
        $i = 0;
        $c = 0;
        $items = [];
        foreach ($this->items->all() as $item) {
            if ($i>=$offset) {
                $c++;
                if ($c<=$limit) {
                    $items[] = $item;
                }
            }
            $i++;
        }
        return $items;
    }
}
