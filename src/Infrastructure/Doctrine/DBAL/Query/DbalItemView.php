<?php
declare(strict_types=1);
namespace Arm\Core\Infrastructure\Doctrine\DBAL\Query;

use Arm\Core\Application\Query\Item;
use Arm\Core\Application\Query\ItemFilter;
use Arm\Core\Application\Query\ItemQuery;
use Doctrine\DBAL\Connection;

class DbalItemView implements ItemQuery
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * DbalItemView constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return int
     */
    public function itemCount(): int
    {
        $result = $this->connection
            ->fetchColumn(
                'SELECT COUNT(i.id) FROM arm_item i join arm_item_type t on t.id = i.type_id'
            );
        return (int)$result;
    }

    /**
     * @param string $code
     * @return Item
     */
    public function itemByCode(string $code): Item
    {
        $result = $this->connection
            ->fetchAssoc(
                '
            SELECT i.id as id, i.code as code, t.code as type_code FROM arm_item i
            JOIN arm_item_type t on t.id = i.type_id
            WHERE i.code = :code',
            [
                ':code' => $code,
            ]
        );
        return new Item(
            $result['id'],
            $result['code'],
            $result['type_code'],
            []
        );
    }

    public function find(ItemFilter $filter): iterable
    {
        $items = [];
        $result = $this->connection
            ->prepare(
                '
            SELECT i.id as id, i.code as code, t.code as type_code FROM arm_item i
            JOIN arm_item_type t on t.id = i.type_id
            LIMIT :limit OFFSET :offset
            ');
        $result->execute([
           ':limit' => $filter->limit(),
           ':offset' => $filter->offset(),
        ]);
        while ($row = $result->fetch()) {
            $items[] = new Item(
                $row['id'],
                $row['code'],
                $row['type_code'],
                []
            );
        }
        return $items;
    }
}
