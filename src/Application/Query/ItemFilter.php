<?php
declare(strict_types=1);
namespace Arm\Core\Application\Query;

final class ItemFilter
{
    /**
     * @var int
     */
    private $offset;

    /**
     * @var int
     */
    private $limit;

    public function __construct()
    {
        $this->offset = 0;
        $this->limit = 10;
    }

    /**
     * @param int $newOffset
     * @return ItemFilter
     */
    public function withOffset(int $newOffset): ItemFilter
    {
        $copy = clone $this;
        $copy->offset = $newOffset;
        return $copy;
    }

    /**
     * @param int $newLimit
     * @return ItemFilter
     */
    public function withLimit(int $newLimit): ItemFilter
    {
        $copy = clone $this;
        $copy->limit = $newLimit;
        return $copy;
    }

    /**
     * @return int
     */
    public function limit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function offset(): int
    {
        return $this->offset;
    }
}
