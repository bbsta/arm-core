<?php
declare(strict_types=1);
namespace Arm\Core\Application\Query;

interface ItemQuery
{
    /**
     * @return int
     */
    public function itemCount(): int;

    /**
     * @param string $code
     * @return Item
     */
    public function itemByCode(string $code): Item;

    /**
     * @param ItemFilter $filter
     * @return iterable|Item[]
     */
    public function find(ItemFilter $filter): iterable;
}
