<?php
declare(strict_types=1);
namespace Arm\Core\Application\Query;

final class Item
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $type;

    /**
     * @var iterable
     */
    private $attributes = [];

    /**
     * Item constructor.
     * @param string $id
     * @param string $code
     * @param string $type
     * @param iterable $attributes
     */
    public function __construct(
        string $id,
        string $code,
        string $type,
        iterable $attributes
    ) {
        $this->id = $id;
        $this->code = $code;
        $this->type = $type;
        $this->attributes = $attributes;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type;
    }

    /**
     * @return iterable
     */
    public function attributes(): iterable
    {
        return $this->attributes;
    }
}
