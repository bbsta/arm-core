<?php
declare(strict_types=1);
namespace Arm\Core\Domain\Items;

class ItemType
{
    /**
     * @var string
     */
    private $code;

    /**
     * ItemType constructor.
     * @param string $code
     */
    public function __construct(string $code)
    {
        $this->code = $code;
    }

    /**
     * @param ItemType $other
     * @return bool
     */
    public function equals(ItemType $other): bool
    {
        return $this->code == $other->code;
    }
}
