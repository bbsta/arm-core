<?php
declare(strict_types=1);
namespace Arm\Core\Domain\Items;

class ItemId
{
    /**
     * @var string
     */
    private $id;

    /**
     * ItemId constructor.
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @param ItemId $other
     * @return bool
     */
    public function equals(ItemId $other): bool
    {
        return $this->id === $other->id;
    }
}
