<?php
declare(strict_types=1);
namespace Arm\Core\Domain\Items;

class ItemCode
{
    /**
     * @var string
     */
    private $code;

    /**
     * ItemCode constructor.
     * @param string $code
     */
    public function __construct(string $code)
    {
        $this->code = $code;
    }

    /**
     * @param string $code
     * @return ItemCode
     */
    public static function fromString(string $code)
    {
        return new self($code);
    }

    /**
     * @param ItemCode $other
     * @return bool
     */
    public function equals(ItemCode $other)
    {
        return $this->code === $other->code;
    }
}
