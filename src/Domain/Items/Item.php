<?php
declare(strict_types=1);
namespace Arm\Core\Domain\Items;

use Domain\Items\Attribute;

class Item
{
    /**
     * @var ItemId
     */
    private $id;

    /**
     * @var ItemCode
     */
    private $code;

    /**
     * @var ItemType
     */
    private $type;

    /**
     * @var Attribute[]
     */
    private $attributes = [];

    /**
     * Item constructor.
     * @param ItemId $itemId
     * @param ItemCode $itemCode
     * @param ItemType $itemType
     * @param iterable $attributes
     */
    public function __construct(
        ItemId $itemId,
        ItemCode $itemCode,
        ItemType $itemType,
        iterable $attributes
    ) {
        $this->id = $itemId;
        $this->code = $itemCode;
        $this->type = $itemType;
        $this->attributes = $attributes;
    }
}
